const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  context: __dirname + '/src',
  entry: {
    'js/app.js': './app.js',
    'css/styles.css': './assets/scss/styles.scss'
  },
  output: {
    path: __dirname + '/build/assets',
    publicPath: '/assets/',
    filename: '[name]'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: [{
          loader: 'vue-loader'
        }]
      },{
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader'
        }]
      },{
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
        })
      },{
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
        use: [{
            loader: "file-loader?name=[name].[ext]&outputPath=fonts/"
        }]
      }
    ]
  },
    plugins: [
        new ExtractTextPlugin({
            filename:  'css/styles.css',
            allChunks: true
        })
    ],
  devServer: {
    contentBase: __dirname + '/src'
  }
};