import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import home from './modules/home/route'
import address from './modules/address/route'

export default new Router({
    routes: [
        home,
        address
    ]
})