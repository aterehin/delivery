export const setCenter = ( state, pos ) => {
    state.center = pos;
}

export const setPlaces = ( state, places ) => {
    state.places = places
}

export const toggle = state => {
    state.opened = !state.opened;
}
