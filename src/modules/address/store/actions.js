import { loaded } from 'vue2-google-maps'

let autocomplete;

loaded.then( () => {
    autocomplete = new google.maps.places.AutocompleteService()
})

export const setCurrentPosition = ( { commit } ) => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            ( pos ) => commit( 'setCenter', {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
            })
        )
    }
}

export const setPlaces = ( { commit }, value ) => {
    if( value !== undefined ) {
        if( autocomplete !== undefined ) {
            autocomplete.getQueryPredictions({
                input: value
            }, ( places ) => commit( 'setPlaces', places ) )
        }
    } else {
        commit( 'setPlaces', [] )
    }
}

export const toggle = ( { commit } ) => commit( 'toggle' )