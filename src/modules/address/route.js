import Address from './components/Address.vue'

export default {
    path: '/address',
    name: 'address',
    component: Address
}